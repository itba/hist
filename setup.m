addpath('./BciVisualToolbox');
addpath('./histogram');
cleanimagedirectory();
% Download signals
fprintf('Database path: %s\n',getdatabasepath())
fprintf('Plot path: %s\n', getimagepath());

% Compile vlfeat.
checkdir='./vlfeat/toolbox/mex/mexmaci64';
if exist(checkdir,'dir')==0
    checkdir='./vlfeat/toolbox/mex/mexw64';
    if exist(checkdir,'dir')==0
        error('Please compile vlfeat library first!');
    end
end
run('./vlfeat/toolbox/vl_setup');

