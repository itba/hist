function basepath=getdatasetpath()

basepath = './signals';
if exist(basepath,'dir')==0
    error('Directory %s not found.  You should download datasets and put them there.\n',basepath);
end

end