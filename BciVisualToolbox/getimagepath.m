function basepath=getimagepath()
        
basepath = sprintf('%s%s%s%s%s%s%s%s','.',filesep,'.',filesep,'Data',filesep,'Plots',filesep);
if exist(basepath,'dir')==0
    mkdir(basepath);
    if exist(basepath,'dir')==0
        error('Plot Directory %s cannot be created.\n',basepath);
    end
end

end