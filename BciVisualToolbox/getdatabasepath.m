function basepath=getdatabasepath()

basepath = sprintf('%s%s%s%s%s%s','.',filesep,'.',filesep,'Data',filesep);
if exist(basepath,'dir')==0
    mkdir(basepath);
    if exist(basepath,'dir')==0
        error('Descriptor Database Directory %s cannot be created.\n',basepath);
    end
end

end