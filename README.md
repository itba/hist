# Histogram of Gradient Orientations of Signal Plots

This Histogram of Gradient Orientations of Signal Plots is a technique to capture the waveform shape and to process EEG signals.

## Matlab

* After cloning, compile VLFeat (see below for instructions)
* Download P300 datasets from BNCI Horizon website [008-2014](http://bnci-horizon-2020.eu/database/data-sets) and from Kaggle Platform [P300-Dataset](https://www.kaggle.com/rramele/p300samplingdataset/version/1) and copy .mat files into:

```
 ./signals/008-2014
 ./signals/p300samplingdataset
```

Run Scripts

```
 bnci_horizon_008_2014
 p300_dataset
```

## VLFeat compiling

The histogram of gradient orientations is calculated by using VLFeat library that must be separately compiled from

```
 ./vlfeat/
```

First, 'mex' needs to be properly configured on Matlab.

```
 mex -setup
```

And after that 'vlfeat' can be compiled from the command line:

Mac OS X (tested on High Sierra, MATLAB 2015b):
```
./vlfeat/compile.sh
```
Windows (tested on Windows 7, VS 2008, MATLAB 2014a):

Open the "Developer Command Prompt for VS2008" and run
```
.\vlfeat\compile.bat
```
If you have further issues, you can check VLFeat online documentation.

## Refs

* http://bnci-horizon-2020.eu/database/data-sets
* https://www.frontiersin.org/articles/10.3389/fnhum.2013.00732/full
* https://www.kaggle.com/rramele/p300samplingdataset/version/1
* https://github.com/vlfeat/vlfeat
