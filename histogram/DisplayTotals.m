function totals = DisplayTotals(subjectRange,globalaccij,globalsigmaaccij,globalauc,globalspeller,channels)

totals = [];
fid = fopen('experiment.log','a');
%{'Subject','Cz','Avg','Best Channel','Value','Stdv'}
for subject=subjectRange
    [ChPer,ChNumPer] = max(globalspeller(subject,:));
    [ChAcc,ChNum] = max(globalaccij(subject,:));
    Stdv = globalsigmaaccij(subject,ChNum);
    
    CMean = mean(globalaccij(subject,:));
    
    % Classification rate @Cz (channel 2)
    Cz = globalaccij(subject,2);
    
    totals = [totals ;[subject Cz CMean ChNum ChAcc ChNumPer ChPer  Stdv]];
    
    fprintf(fid,'%d     & %6.2f', [ subject Cz]);
    fprintf(fid,'& %s', channels{ChNumPer});
    fprintf(fid,'& %6.4f $\\pm$ %4.4f \\\\\n', [ChPer Stdv]);
end
fclose(fid);

end
