% EEG(subject,trial,flash)
function [EEG, labelRange, stimRange] = prepareEEG(Fs, windowsize, downsize, flashespertrial, subjectRange,channelRange)

artifactcount = 0;   

channels={ 'Fz'  ,  'Cz',    'P3' ,   'Pz'  ,  'P4'  , 'PO7'   , 'PO8',  'Oz'};

bandpass = true;
            
for subject=subjectRange
    clear data.y_stim
    clear data.y
    clear data.X
    clear data.trial

    load(sprintf('%s%sp300samplingdataset%sP300S%02d.mat',getdatasetpath(),filesep,filesep,subject));

    dataX = data.X;
    dataX = notchsignal(data.X, channelRange,Fs);
    datatrial = data.trial;

    if (bandpass)
        dataX = bandpasseeg(dataX, channelRange,Fs,3);
    end
    
    dataX = decimatesignal(dataX,channelRange,downsize); 
     
    for trial=1:size(datatrial,2)
        for flash=1:flashespertrial
            
            start = data.flash((trial-1)*120+flash,1);
            duration = data.flash((trial-1)*120+flash,2);
            
            % Check overflow of the EEG matrix
            if (ceil(Fs/downsize)*windowsize>size(dataX,1)-ceil(start/downsize))
                dataX = [dataX; zeros(ceil(Fs/downsize)*windowsize-size(dataX,1)+ceil(start/downsize)+1,8)];
            end
          
            output = extract(dataX, ...
                (ceil(start/downsize)), ...
                floor(Fs/downsize)*windowsize);
            
            % Piecewise baseline removal
            %output=bf(output,1:5:size(output,2));

            EEG(subject,trial,flash).stim = data.flash((trial-1)*120+flash,3);
            EEG(subject,trial,flash).label = data.flash((trial-1)*120+flash,4);
            
            [trial, flash, EEG(subject,trial,flash).stim, EEG(subject,trial,flash).label]
            
            EEG(subject,trial,flash).isartifact = false;
            artifact = isartifact(output,70);
            if (artifact)
                artifactcount = artifactcount + 1;
                EEG(subject,trial,flash).isartifact = true;
            end
            
            % This is a very important step, do not forget it.
            % Substract the media from the epoch.
            [n,m]=size(output);
            output=output - ones(n,1)*mean(output,1); 

            EEG(subject,trial,flash).EEG = output;

        end
    end
end

end